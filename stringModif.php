<?php
/*
//Entourer l'expression de delimiteurs tq / # etc tout peut etre utiliser
// options après le deuxieme delimiteur : i insensible a la casse, g répéter le remplacement
// remplacement d'une info :
$chaine = 'les chiens et les CHATS sont méchants';
preg_replace('/chiens|chats/gi', 'dragons', $chaine);

// Chercher une information
preg_match($search, $subjet);
//retourne true si chaine trouvée et false ds le cas contraire

//on peut aussi identifier ce qui est cherché
preg_match('/5e/','Vous êtes 5e',$match);
//val retournée $match[0]='5e'


preg_match('/(5)e/','Vous êtes 5e',$match);
//val retournée $match[0]='5e' $match[1]='5'


preg_match('/([0123456789])e/','Vous êtes 5e',$match);
preg_match('/([0-9])e/','Vous êtes 5e',$match);
//val retournée $match[0]='5e' $match[1]='5'


preg_match('/([0-9]+)e/','Vous êtes 15e',$match);
//val retournée $match[0]='15e' $match[1]='15'

//identifier "Vous êtes 1re, 2e, 3e, etc
preg_match('/([0-9]+)r?e/','Vous êtes 1re',$match);

//identifier "Vous êtes entre 100e et 1000e
preg_match('/([0-9]{3)e/','Vous êtes 100e',$match);

//identifier "Vous êtes entre 100e et 10000e
preg_match('/([0-9]{3,4)e/','Vous êtes 100e',$match);

// identifier " Vous êtes au moins le 100e"
preg_match('/([0-9]{3,)e/','Vous êtes 100e',$match);

*/

/* mini exercice
$date = '12-05-2018';
preg_match('/^([0-3][0-9])-([0-1][0-9])-([0-9]{4})$/', $date, $match);
echo '<pre>';
print_r($match);
echo '</pre>';
*/


/* Mini exercice
$num = '022 123 45 67';
$num2 = '0041221234567';
$num3 = '+4122-123-45-67';

echo 'Num1';
$replace = preg_replace('/[^0-9]/', '', $num);
echo '<pre>';
print_r($replace);
echo '</pre>';

echo 'Num2';
$replace = preg_replace('/[^0-9]/', '', $num2);
echo '<pre>';
print_r($replace);
echo '</pre>';

echo 'Num3';
$replace = preg_replace('/[^0-9]/', '', $num3);
echo '<pre>';
print_r($replace);
echo '</pre>';
*/

// version avec match :
//   ((00|\+)41|0)22[-\s]*[0-9]{3}[-\s]*[0-9]{2}[-s]*[0-9]{2}
//foutre url
$content = file_get_contents('https://www.imdb.com/title/tt4972582/?ref_=nv_sr_1');
//echo $content;
//titre
preg_match('/\"url\"\: \"\/title\/(.*)\/\"\,\s*\"name\"\: \"(.*)\"/', $content, $match);

echo '<pre>';
print_r($match);
echo '</pre>';

echo 'TITRE : '.$match[2].'<br><br><br>';

//personnes
echo 'PERSONNES : <br>';
preg_match_all('/\"url\"\: \"\/name\/(.*)\/\"\,\s*\"name\"\: \"(.*)\"/', $content, $match);

echo '<pre>';
print_r($match[2]);
echo '</pre>';


//date
preg_match('/\"datePublished\"\: \"(.*)\"/', $content, $match);

echo 'DATE : '.$match[1];

//note

preg_match('/\"ratingValue\"\: \"(.*)\"/', $content, $match);
//echo '<pre>';
//print_r($match);
//echo '</pre>';
echo '<br>Note : '.$match[1];

//  affiche
preg_match('/src=\"(.*)\.jpg\"/', $content, $match);
//echo '<pre>';
//print_r($match);
//echo '</pre>';

echo' <br> <br>IMAGE: <br><img src="'.$match[1].'.jpg">';

