<?php

session_start();
if (isset($_SESSION['dates_event']) && $_SESSION['dates_event']) {
    $eventRequest = $_SESSION['dates_event'];
} else {
    $eventRequest = array();
}
//verif si nouvel event et enregistrement
if (isset($_REQUEST['date']) && $_REQUEST['date']) {

    $dateNewEvent = DateTime::createFromFormat('Y-m-d', $_REQUEST['date']);


    array_push($eventRequest, $dateNewEvent);
    //save dans sessions
    $_SESSION['dates_event'] = $eventRequest;

    //array_push($_SESSION['dates_event'],$eventRequest);
}

//verif si date deja save dans sessions
//if (isset($_SESSION['dates_event']) && $_SESSION['dates_event']) {
//    foreach ($_SESSION['dates_event'] as $dates) {
//
//
//        $result = $dates->format('Y-m-d H:i:s');
//        $dateNewEvent = DateTime::createFromFormat('Y-m-d', $result);
//
//        array_push($eventRequest, $dateNewEvent);
//        echo "event request";
//        echo '<pre>';
//        print_r($_SESSION['dates_event']);
//       // $_SESSION['dates_event'] = $eventRequest;
//        echo '</pre>';
//    }
//}


//if (!isset($_SESSION['login_datetime'])) {
//    $_SESSION['login_datetime'] = date('c');
//}
//creation du cookie si pas existant pour recuperer le mois choisi


if (isset($_GET['month'])) {
    //si parametre
    $month = $_GET['month'];
    //enregistrement dans un cookie
    setcookie('month_chosen', $month, time() + 60 * 60 * 24 * 30); // 30 jours
} elseif (isset($_COOKIE['month_chosen'])) {
    //sinon si un cookie existe
    $month = $_COOKIE['month_chosen'];
} else {
    //sinon mois courant
    $month = date('m');
    setcookie('month_chosen', $month, time() + 60 * 60 * 24 * 30); // 30 jours
}

$nextMonth = $month + 1;
$lastMonth = $month - 1;
//gestion erreurs sur le mois 12 et 1 (reset)
if ($nextMonth > 12) {
    $nextMonth = 1;
}
if ($lastMonth < 1) {
    $lastMonth = 12;
}
?>

<html lang="en" class="">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex">
    <style class="cp-pen-styles" type="text/css">
        * {
            -webkit-font-smoothing: antialiased;
        }

        body {
            font-family: 'helvetica neue';
            background-color: #A25200;
            margin: 0;
        }

        .wrapp {
            width: 450px;
            margin: 30px auto;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-content: center;
            align-items: center;
            box-shadow: 0 0 10px rgba(54, 27, 0, 0.5);
        }

        .flex-calendar .days, .flex-calendar .days .day.selected, .flex-calendar .month, .flex-calendar .week {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
        }

        .flex-calendar {
            width: 100%;
            min-height: 50px;
            color: #FFF;
            font-weight: 200
        }

        .flex-calendar .month {
            position: relative;
            display: flex;
            flex-direction: row;
            flex-wrap: nowrap;
            -webkit-justify-content: space-between;
            justify-content: space-between;
            align-content: flex-start;
            align-items: flex-start;
            background-color: #ffb835;
        }

        .flex-calendar .month .arrow, .flex-calendar .month .label {
            height: 60px;
            order: 0;
            flex: 0 1 auto;
            align-self: auto;
            line-height: 60px;
            font-size: 20px;
        }

        .flex-calendar .month .arrow {
            width: 50px;
            box-sizing: border-box;
            background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAABqUlEQVR4Xt3b0U3EMBCE4XEFUAolHB0clUFHUAJ0cldBkKUgnRDh7PWsd9Z5Tpz8nyxFspOCJMe2bU8AXgG8lFIurMcurIE8x9nj3wE8AvgE8MxCkAf4Ff/jTEOQBjiIpyLIAtyJpyFIAjTGUxDkADrjhxGkAIzxQwgyAIPxZgQJAFJ8RbgCOJVS6muy6QgHiIyvQqEA0fGhAArxYQAq8SEASvHTAdTipwIoxk8DUI2fAqAc7w6gHu8KkCHeDSBLvAtApng6QLZ4KkDGeBpA1ngKQOb4YYDs8UMAK8SbAVaJNwGsFN8NsFq8FeADwEPTmvPxSXV/v25xNy9fD97v8PLuVeF9FiyD0A1QKVdCMAGshGAGWAVhCGAFhGGA7AgUgMwINICsCFSAjAh0gGwILgCZENwAsiC4AmRAcAdQR5gCoIwwDUAVYSqAIsJ0ADWEEAAlhDAAFYRQAAWEcIBoBAkAIsLX/rV48291MgAEhO747o0Rr82J23GNS+6meEkAw0wwx8sCdCAMxUsDNCAMx8sD/INAiU8B8AcCLT4NwA3CG4Az68/xOu43keZ+UGLOkN4AAAAASUVORK5CYII=) no-repeat;
            background-size: contain;
            background-origin: content-box;
            padding: 15px 5px;
            cursor: pointer;
        }

        .flex-calendar .month .arrow:last-child {
            -webkit-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg);
        }

        .flex-calendar .month .arrow.visible {
            opacity: 1;
            visibility: visible;
            cursor: pointer;
        }

        .flex-calendar .month .arrow.hidden {
            opacity: 0;
            visibility: hidden;
            cursor: default;
        }

        .flex-calendar .days, .flex-calendar .week {
            line-height: 25px;
            font-size: 16px;
            display: flex;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
        }

        .flex-calendar .days {
            background-color: #FFF;
        }

        .flex-calendar .week {
            background-color: #faac1c;
        }

        .flex-calendar .days .day, .flex-calendar .week .day {
            flex-grow: 0;
            -webkit-flex-basis: calc(100% / 7);
            min-width: calc(100% / 7);
            text-align: center;
        }

        .flex-calendar .days .day {
            min-height: 60px;
            box-sizing: border-box;
            position: relative;
            line-height: 60px;
            border-top: 1px solid #FCFCFC;
            background-color: #fff;
            color: #8B8B8B;
            -webkit-transition: all .3s ease;
            transition: all .3s ease;
        }

        .flex-calendar .days .day.out {
            background-color: #fCFCFC;
        }

        .flex-calendar .days .day.disabled.today, .flex-calendar .days .day.today {
            color: #FFB835;
            border: 1px solid;
        }

        .flex-calendar .days .day.selected {
            display: flex;
            flex-direction: row;
            flex-wrap: nowrap;
            -webkit-justify-content: center;
            justify-content: center;
            align-content: center;
            -webkit-align-items: center;
            align-items: center;
        }

        .flex-calendar .days .day.selected .number {
            width: 40px;
            height: 40px;
            background-color: #FFB835;
            border-radius: 100%;
            line-height: 40px;
            color: #FFF;
        }

        .flex-calendar .days .day:not(.disabled):not(.out) {
            cursor: pointer;
        }

        .flex-calendar .days .day.disabled {
            border: none;
        }

        .flex-calendar .days .day.disabled .number {
            background-color: #EFEFEF;
            background-image: url(data:image/gif;base64,R0lGODlhBQAFAOMAAP/14////93uHt3uHt3uHt3uHv///////////wAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAAAALAAAAAAFAAUAAAQL0ACAzpG0YnonNxEAOw==);
        }

        .flex-calendar .days .day.event:before {
            content: "";
            width: 6px;
            height: 6px;
            border-radius: 100%;
            background-color: #faac1c;
            position: absolute;
            bottom: 10px;
            margin-left: -3px;
        }
    </style>

    <title>Calendar</title>
</head>

<body>

    <div class="wrapp">
        <div class="flex-calendar">
            <div class="month">
                <?php
                echo "<a href=\"?month=" . $lastMonth . "\" class=\"arrow visible\"></a>"
                ?>

                <div class="label">
                    <?php
                    $dateObj = DateTime::createFromFormat('!m', $month);
                    $monthName = $dateObj->format('F'); // March
                    echo $monthName . " " . date('Y');
                    ?>
                </div>

                <?php
                echo "<a href=\"?month=" . $nextMonth . "\" class=\"arrow visible\"></a>"
                ?>
            </div>

            <div class="week">
                <div class="day">M</div>
                <div class="day">T</div>
                <div class="day">W</div>
                <div class="day">T</div>
                <div class="day">F</div>
                <div class="day">S</div>
                <div class="day">S</div>
            </div>

            <?php


            //Calcul nbr jour dans mois actuel de l'annee actuelle
            $numberDaysInMonths = cal_days_in_month(CAL_GREGORIAN, date($month), date('Y')); // 31

            //Remplissage tableau avec nbrs jours du mois
            $joursDuMois = array();

            for ($i = 0; $i < $numberDaysInMonths; $i++) {
                $joursDuMois[] = $i + 1;
            }

            ?>
            <div class="days">
                <?php
                //recuperation premier jour de la semaine du mois (en int)
                $dayofweek = date('w', strtotime(date('Y-' . $month . '-01'))); //4

                //Affichage des jours vide avant le premier jour
                for ($i = 1; $i < $dayofweek; $i++) {
                    echo "
                            <div class=\"day out\">
                                <div class=\"number\"></div>
                            </div>
                        ";
                }


                //Recuperation jour actuel
                $currentDay = DateTime::createFromFormat('d-m', date('d-m'));


                //Creation dune petite liste d'événements et de jours disabled
                $dateDisabled1 = DateTime::createFromFormat('d-m', '01-03');
                $dateDisabled2 = DateTime::createFromFormat('d-m', '03-04');
                $dateDisabled3 = DateTime::createFromFormat('d-m', '08-03');
                $disabled = array($dateDisabled1, $dateDisabled2, $dateDisabled3);

                $dateEvent1 = DateTime::createFromFormat('d-m', '03-03');
                $dateEvent2 = DateTime::createFromFormat('d-m', '04-05');
                $dateEvent3 = DateTime::createFromFormat('d-m', '02-04');
                $events = array($dateEvent1, $dateEvent2, $dateEvent3);

                foreach ($eventRequest as $dateEvent) {
                    //$dateEvent->format('d-m');
                    array_push($events, $dateEvent);
                }


                foreach ($joursDuMois as $jour) {
                    $classes = 'day'; //variable pour builder la chaine de caractere définissant la classe

                    $jourParcouru = DateTime::createFromFormat('d-m', $jour . '-' . $month);

                    if ($currentDay == $jourParcouru) {
                        $classes .= " selected";
                    }
                    if (in_array($jourParcouru, $disabled)) {
                        $classes .= " disabled";
                    }
                    if (in_array($jourParcouru, $events)) {
                        $classes .= " event";
                    }

                    //affichage
                    echo '<div class="' . $classes . '"><div class="number">' . $jour . '</div></div>';
                }

                ?>

            </div>
            <?php
            echo '<pre>';
            print_r($events);
            echo '</pre>';
            ?>
            <form method="post" action="calendrier.php" enctype="multipart/form-data">

                <p>
                    <label for="date">Date event</label>
                    <input type="date" name="date" id="date" value="">
                </p>
                <p>
                    <label for="image-event">Image event</label>
                    <input type="file" name="image-event" id="image-event" value="">
                </p>
                <p>
                    <button type="submit">Envoyer</button>
                </p>
            </form>
        </div>
    </div>
</body>
</html>