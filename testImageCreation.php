<?php
//creer une image
$im = imagecreatetruecolor(400,300);
$text_color = imagecolorallocate($im, 145, 218, 15);

// Ajout Texte
imagestring($im, 5,20,20, utf8_decode('Wake up, Néo!'), $text_color);
imagestring($im, 5,20,40, '...', $text_color);
imagestring($im, 5,20,60, '...', $text_color);
imagestring($im, 5,20,90, 'Follow the white rabbit', $text_color);

// Affichage
header('Content-Type: image/jpeg');
imagejpeg($im, NULL,90);
imagedestroy($im);
