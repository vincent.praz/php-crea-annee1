<?php
//Ouverture image origine
$im = imagecreatefromjpeg("img/testNeg.jpg");

//Ajout du filtre négatif
//imagefilter($im,IMG_FILTER_NEGATE);

//Relief
imagefilter($im,IMG_FILTER_EMBOSS);
imagefilter($im,IMG_FILTER_EDGEDETECT);
imagefilter($im,IMG_FILTER_COLORIZE,20,0,0);


//enregistrement de l'image dans un nouveau fichier
imagejpeg($im,"img/testNeg-negate.jpg",90);


//liberation de la memoire
imagedestroy($im);