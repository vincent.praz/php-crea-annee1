<?php
//Création variable br pour ajouter des br partout
$br = "<br>";

echo "<h1>tamer</h1>";
echo date('d/m/Y H:i:s') . $br;


//Création d'un array
$starwars_movies = array('Un nouvel espoir', 'tavuctrobienstarwars', 'wow scifi futur de ouf');
echo $starwars_movies[1] . $br . $br;


//Boucle foreach tableau as elements
foreach ($starwars_movies as $elements) {
    echo $elements . $br;
}

echo $br;

//tableaux associatifs
$first_starwars_movie = array(
    'title' => 'un Nouvel espoir de folie',
    'year' => 1977,
    'director' => 'George Lucas'
);

//Affichage tableau associatif
echo "Le premier film s'appelle " . $first_starwars_movie['title'] . ", réalisé par "
    . $first_starwars_movie['director'] . "et sorti en " . $first_starwars_movie['year'];

//Tableau multidimentionnels
$starwars_movie_list = array(
    array(
        'title' => 'un Nouvel espoir de folie',
        'year' => 1977
    ),
    array(
        'title' => "L'Empire contre-attaque",
        'year' => 1980
    ),
    array(
        'title' => 'Le retour du jedi',
        'year' => 1983
    ),
);

echo $br . $br;

//Foreach affichage tableau multidimentionnels
foreach ($starwars_movie_list as $elements) {
    echo $elements["title"] . " : " . $elements["year"] . $br;
}

//fonctions sur les tableaux

echo array_search('Un nouvel espoir', $starwars_movies); //Retourne l'index ou il est trouvé

//Tri tableau par ordre alphabétique
sort($starwars_movies);

//Ajouter un élément a la fin du tableau
array_push($starwars_movies, "Le reveil de la Force");
$starwars_movies[] = "Le reveil dela Force";

//Colle les éléments du tableau avec une virgule
$line = implode(',', $starwars_movies);

//Transforme une chaine de caractère en tableau (séparateur virgule)
$tab2 = explode(',', $line);


//Les fonctions
/**
 * Fonction get infos qui va retourner les informations sur film choisi
 * @param $title
 * @return array|mixed
 */
function get_infos($title)
{
    $starwars_movies_fct = array(
        array(
            'title' => 'un Nouvel espoir de folie',
            'year' => 1977
        ),
        array(
            'title' => "L'Empire contre-attaque",
            'year' => 1980
        ),
        array(
            'title' => 'Le retour du jedi',
            'year' => 1983
        ),
    );
    $result = array();
    foreach ($starwars_movies_fct as $movie) {
        if ($movie['title'] == $title) {
            $result = $movie;
        }
    }
    return $result;
}

//TODO CHEATE POUR LAFFICHAGE <PRE> ET PRINTR
echo'<pre>';
print_r($starwars_movies);
echo'</pre>';

//Passage par référence
function addition(&$valeur){
    $valeur +=2;
}

$a = 1;
addition($a);
echo $a;

//inclusion de headers etc
//include/include_once si fichier existe pas juste warning mais fonctionne
include('header.php');

//require/require_once bloque si fichier existe pas
//require('header.php');


session_start();
if(!isset($_SESSION['login_datetime'])){
    $_SESSION['login_datetime'] = date('c');
}

if(!isset($_COOKIE['login_datetime'])){
    setcookie('login_datetime',date('c'),time()+60*60*24*30);//30 jours
    echo "Premiere connexion : ".$_COOKIE['login_datetime'];
    echo "Premiere connexion session : ".$_SESSION['login_datetime'];
}
session_abort();

//$_GET $_POST, $_REQUEST permet de recuperer les 2 méthodes
?>

<form method="post" action="random.php">
    <label for="nom">Nom</label>
    <input type="text" name="nome" id="nom" value="">
    <button type="submit">Envoyer</button>
</form>
<?php
//htmlspecialchars() et striptags
?>