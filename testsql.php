<?php

// Connexion
try {
    $db = new PDO('mysql:host=localhost:3306;dbname=calendrier;charset=utf8',
        'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

//  Lecture
$response = $db->query('SELECT * FROM events');
while ($infos = $response->fetch(PDO::FETCH_ASSOC)) {
    echo '<pre>';
    print_r($infos);
    echo '</pre>';
}

//requete preparee cela va antislasher la valeur titre pour la proteger des injections
$query = $db->prepare('SELECT * FROM events WHERE title = :title');
$title = 'test';
if ($response = $query->execute(array(':title' => $title))) {
    $infos = $query->fetch(PDO::FETCH_ASSOC);
    echo '<pre>';
    print_r($infos);
    echo '</pre>';
}


//Ecriture
//forme 1
//$db->exec("INSERT INTO events(date,title,image_name) VALUES('2018-04-25','cours php crea','lol.jpg')");
//forme 2
//$db->exec("INSERT INTO events SET date='2018-04-25', title='cours php crea', image_name='lol.jpg'");

//mise a jour
//$db->exec("UPDATE events SET title = 'wow' WHERE image_name='lol.jpg'");

