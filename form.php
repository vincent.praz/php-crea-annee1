<form method="post" action="form.php" enctype="multipart/form-data">
    <p>
        <label for="nom">Nom</label>
        <input type="text" name="nom" id="nom" value="">
    </p>
    <p>
        <label for="email">E-mail</label>
        <input type="email" name="email" id="email" value="">
    </p>
    <p>
        <label for="serie_tv">Série TV à voir</label>
        <input type="text" name="serie_tv" id="serie_tv" value="">
    </p>
    <p>
        <input type="file" name="sous-titres" id="sous-titres" value="">
    </p>
    <p>
        <button type="submit">Envoyer</button>
    </p>
</form>

<?php
if ( isset( $_REQUEST['nom'] ) && $_REQUEST['nom'] )
    echo '<p>Votre nom: ' . $_REQUEST['nom'] . '</p>';
if ( isset( $_REQUEST['email'] ) && $_REQUEST['email'] )
    echo '<p>Votre adresse e-mail: ' . $_REQUEST['email'] . '</p>';
if ( isset( $_REQUEST['serie_tv'] ) && $_REQUEST['serie_tv'] )
    echo '<p>La série que vous conseillez: ' . $_REQUEST['serie_tv'] . '</p>';

//Fichier viens dans $_FILE[nomduchamp] avec les datas :
/*
 * name
 * type
 * size
 * tempname
 * error ?
 */

//pour faciliter les debugs:::
echo '<pre>';
print_r(debug_backtrace());
echo '</pre>';

//avoir debug uniquement sur son ip
if($_SERVER['REMOTE_ADDR']=='1.2.3.4'){
    echo '<pre>';print_r($var);echo '</pre>';
}