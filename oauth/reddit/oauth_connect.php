<?php

require_once "config.php";

$url = REDDIT_OAUTH_URL .
    //identifie l'app qui fait la requete
    '/?client_id=' . REDDIT_CONSUMER_KEY .
    // ou rediriger lutilisateur avec le code -> encoder lurl pour eviter des problemes avec carac spéciaux
    '&redirect_uri=' . urlencode(REDDIT_CALLBACK_URL) .
    //type de demande (toujours code)
    '&response_type=code' .
    //duree de connexion (temp ou perma)
    '&duration=permanent' .
    //permissions quon cherche a obtenir
    '&scope=identity' .
    //element (generalement) aleatoire que lon compare apres
    '&state=' . time() . rand(0, 10000000);

header("location:".$url);