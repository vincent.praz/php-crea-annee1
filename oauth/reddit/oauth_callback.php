<?php

session_start();

require_once 'config.php';

function redirectToConnect()
{
    header("location:oauth_connect.php");
    die();
}

if (!isset($_GET['code'])) {
    redirectToConnect();
}

//code renvoyé par reddit
$codeToCheck = $_GET['code'];

//Création des paramètres de validations
$parameters = http_build_query([
    'grant_type' => 'authorization_code',
    'code' => $codeToCheck,
    'redirect_uri' => REDDIT_CALLBACK_URL
]);

$appAuth = base64_encode(REDDIT_CONSUMER_KEY . ":" . REDDIT_CONSUMER_SECRET);

// Création d'un contexte pour une requete http
$context = stream_context_create([
    'http' => [
        'method' => 'POST',
        'header' => [
            'Content-Type: application/x-www-form-urlencoded', //données encodée
            "Authorization: Basic $appAuth", //authorisation de l'app
        ],
        'content' => $parameters // ajout de nos paramètres
    ]
]);

// recuperation
$userResponse = file_get_contents(REDDIT_TOKEN_URL, false, $context);

// decodage + stockage
$_SESSION['user_response'] = json_decode($userResponse);

//var_dump($_SESSION);

//var_dump($userResponse);