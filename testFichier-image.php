<?php

//Gestion de fichiers
//Lecture
$handle = fopen("lol.txt", "r");
if ($handle) {
    while (($buffer = fgets($handle, 4096)) !== false) {
        echo $buffer;
    }

    fclose($handle);
}

//ecriture
$handle = fopen("lol.txt", "a+");
if ($handle) {
    if (fwrite($handle, 'philippe') === false) {
        die("Impossible d'écrire dans le fichier");
    }

    fclose($handle);
}

// CF Cours pour principaux modes d'ouverture avec fopen()


// lecture 2
$lol = file_get_contents("lol.txt");
// nl2br pour remplacer les \n en br html lus dans le fichier
echo nl2br($lol);

//remplace le contenu ecritrue
file_put_contents("lol.txt","mdr");

//ajouter a la fin du fichier
file_put_contents("lol.txt","\nmdr",FILE_APPEND);

//manipuler des images avec la librairie GD ou imagemaagick
//chargement en memoire d'une image
//todo: ajouter des variable tq : $im = imagecreatefrompng($file) pour garder en memoire
imagecreatefrompng($file);
imagecreatefromjpeg($file);
imagecreatefromgif($file);

//création d'une image vide
imagecreatetruecolor($width,$height);

//on defini des couleurs
imagecolorallocate($image,$red,$green,$blue);

//on ecrit des textes
imagestring($image,$font,$x,$y,$color);

//decouper et redimensionner
//imagecopyresized($source,$destination,...);

//fusion de 2 images (watermark)
//imagecopy($image,$stamp,...).

//sauvegarde
imagepng($im,"test.png");

//on affiche dans le navigateur
header("Content-type:image/png");
imagepng($im);

// CF cours pour exemples
